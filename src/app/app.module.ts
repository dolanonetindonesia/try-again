import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { MaterialModule } from './material.module';
import { RegCorporateComponent } from './components/reg-corporate/reg-corporate.component';
import { AppRoutingModule } from './/app-routing.module';
import { RegPersonalComponent } from './components/reg-personal/reg-personal.component';
import { AccountSuccessComponent } from './components/status/account-success/account-success.component';
import { HomeComponent } from './components/dashboard/home/home.component';
import { PendaftaranAwalPersonalComponent } from './components/tnc/pendaftaran-awal-personal/pendaftaran-awal-personal.component';
import { PendaftaranAwalCorporateComponent } from './components/tnc/pendaftaran-awal-corporate/pendaftaran-awal-corporate.component';
import { LoginComponent } from './components/auth/login/login.component';
import { ForgetPasswordComponent } from './components/auth/forget-password/forget-password.component';
import { StatusHeadComponent } from './components/status/status-head/status-head.component';
import { StatusFootComponent } from './components/status/status-foot/status-foot.component';
import { RegisterSuccessComponent } from './components/status/register-success/register-success.component';
import { AccountActivationComponent } from './components/auth/account-activation/account-activation.component';
import { AccountFailedComponent } from './components/status/account-failed/account-failed.component';
import { ForgetPasswordChangeComponent } from './components/auth/forget-password-change/forget-password-change.component';
import { AuthInterceptor } from './components/auth/auth.interceptor';
import { RegisterFailedComponent } from './components/status/register-failed/register-failed.component';
import { InformasiPribadiComponent } from './components/dashboard/akun-saya/informasi-pribadi/informasi-pribadi.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    RegCorporateComponent,
    RegPersonalComponent,
    AccountSuccessComponent,
    HomeComponent,
    PendaftaranAwalPersonalComponent,
    PendaftaranAwalCorporateComponent,
    LoginComponent,
    ForgetPasswordComponent,
    StatusHeadComponent,
    StatusFootComponent,
    RegisterSuccessComponent,
    AccountActivationComponent,
    AccountFailedComponent,
    ForgetPasswordChangeComponent,
    RegisterFailedComponent,
    InformasiPribadiComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export class SharedModule { }