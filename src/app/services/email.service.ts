import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICheckMail } from '../interfaces/email';
import { catchError, map, tap, filter, delay } from 'rxjs/operators';
// import 'rxjs/operator/map';
// import 'rxjs/operator/filter';
// import 'rxjs/operator/delay';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'apikey': 'gzg75aFGyQNf9aT5yzONLVZQk5XV4Ryz'
  })
};

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  // baseUrl = "http://dolanoserver:8081/";
  baseUrl = "http://dolanoserver:8000/";

  constructor(private http: HttpClient) { }

  cekEmail(email): Observable<ICheckMail[]>{
    return this.http.post<ICheckMail[]>(this.baseUrl + 'partner/email/'+ email, httpOptions)
  }
  
}
