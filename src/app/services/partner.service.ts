import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRegister } from '../interfaces/partner';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'apikey': 'gzg75aFGyQNf9aT5yzONLVZQk5XV4Ryz'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  baseUrl = "http://dolanoserver:8000/";

  constructor(
    private http: HttpClient
  ) { }

  regPartner(data){
    return this.http.post(this.baseUrl+"partner/register", data, httpOptions);
  }

  addPartnerCredential(token){
    return this.http.get(this.baseUrl + 'partner/account-activation/' + token, httpOptions);
  }

}
