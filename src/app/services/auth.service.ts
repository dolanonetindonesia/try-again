import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/shareReplay'
import { map, share, shareReplay } from 'rxjs/operators';
import { IAuth, IUser } from '../interfaces/partner';
import * as moment from "moment";

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'apikey': 'gzg75aFGyQNf9aT5yzONLVZQk5XV4Ryz'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = "http://dolanoserver:8000/";

  constructor(private http: HttpClient) { }

  login(data){
    return this.http.post<any>(this.baseUrl + 'auth/login/', data, httpOptions)

      // .do(res => this.setSession)
      // .shareReplay()

    // return this.http.post<IAuth[]>(this.baseUrl + 'auth/user/', data, httpOptions)
    //            .pipe(map((res:any) => {
    //              if(res && res.token){
    //                console.log('success');
    //              }
    //            }))
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }

  // private setSession(authResult){
  //   const expiresAt = moment().add(authResult.expiresAt, 'second');
  //   localStorage.setItem('id_token', authResult.idToken);
  //   localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
  // }

  // logout() {
  //   localStorage.removeItem("id_token");
  //   localStorage.removeItem("expires_at");
  // }

  // public isLoggedIn() {
  //   return moment().isBefore(this.getExpiration());
  // }

  // isLoggedOut() {
  //   return !this.isLoggedIn();
  // }

  // getExpiration() {
  //   const expiration = localStorage.getItem("expires_at");
  //   const expiresAt = JSON.parse(expiration);
  //   return moment(expiresAt);
  // }


}
