export interface IRegister{
    tipe_partner: number,
    first_name: string,
    last_name: string,
    phone_number: string,
    email: string,
    password: string
}

export interface IAuth{
    email: string,
    password: string,
    role: string,
}

export interface IUser{
    email: string,
    password: string
}