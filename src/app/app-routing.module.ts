import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RegCorporateComponent } from './components/reg-corporate/reg-corporate.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { RegPersonalComponent } from './components/reg-personal/reg-personal.component';
import { AccountSuccessComponent } from './components/status/account-success/account-success.component';
import { PendaftaranAwalPersonalComponent } from './components/tnc/pendaftaran-awal-personal/pendaftaran-awal-personal.component';
import { PendaftaranAwalCorporateComponent } from './components/tnc/pendaftaran-awal-corporate/pendaftaran-awal-corporate.component';
import { LoginComponent } from './components/auth/login/login.component';
import { ForgetPasswordComponent } from './components/auth/forget-password/forget-password.component';
import { HomeComponent } from './components/dashboard/home/home.component';
import { RegisterSuccessComponent } from './components/status/register-success/register-success.component';
import { AccountActivationComponent } from './components/auth/account-activation/account-activation.component';
import { AccountFailedComponent } from './components/status/account-failed/account-failed.component';
import { ForgetPasswordChangeComponent } from './components/auth/forget-password-change/forget-password-change.component';
import { RegisterFailedComponent } from './components/status/register-failed/register-failed.component';
import { InformasiPribadiComponent } from './components/dashboard/akun-saya/informasi-pribadi/informasi-pribadi.component';

const routes: Routes = [
  { path: 'registration', component: RegistrationComponent },
  { path: 'registration/status/account-success', component: AccountSuccessComponent },
  { path: 'registration/status/account-failed', component: AccountFailedComponent },
  { path: 'registration/auth/account-activation/:token', component: AccountActivationComponent },
  { path: 'registration/status/register-success', component: RegisterSuccessComponent },
  { path: 'registration/status/register-failed', component: RegisterFailedComponent },
  { path: 'tnc/individu', component: PendaftaranAwalPersonalComponent },
  { path: 'tnc/perusahaan', component: PendaftaranAwalCorporateComponent },
  { path: 'login', component: LoginComponent },
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'forget-password-change', component: ForgetPasswordChangeComponent },
  { path: 'dashboard/home', component: HomeComponent },
  { path: 'dashboard/home/akun-saya/informasi-pribadi', component: InformasiPribadiComponent },
  { path: '', redirectTo: 'registration', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
    
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
