import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaranAwalCorporateComponent } from './pendaftaran-awal-corporate.component';

describe('PendaftaranAwalCorporateComponent', () => {
  let component: PendaftaranAwalCorporateComponent;
  let fixture: ComponentFixture<PendaftaranAwalCorporateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaranAwalCorporateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaranAwalCorporateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
