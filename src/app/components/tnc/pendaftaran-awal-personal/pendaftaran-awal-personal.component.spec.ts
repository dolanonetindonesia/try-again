import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaranAwalPersonalComponent } from './pendaftaran-awal-personal.component';

describe('PendaftaranAwalPersonalComponent', () => {
  let component: PendaftaranAwalPersonalComponent;
  let fixture: ComponentFixture<PendaftaranAwalPersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaranAwalPersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaranAwalPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
