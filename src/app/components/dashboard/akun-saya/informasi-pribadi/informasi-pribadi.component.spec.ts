import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformasiPribadiComponent } from './informasi-pribadi.component';

describe('InformasiPribadiComponent', () => {
  let component: InformasiPribadiComponent;
  let fixture: ComponentFixture<InformasiPribadiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformasiPribadiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformasiPribadiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
