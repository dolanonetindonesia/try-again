import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { InformasiPribadiComponent } from '../akun-saya/informasi-pribadi/informasi-pribadi.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mode = new FormControl('side');
  opened:boolean = true;

  constructor(
    public dialog: MatDialog
  ) { 
  }

  ngOnInit() {
  }

  edit_dataDiri(){
    const dialconf = new MatDialogConfig();

    dialconf.maxHeight = "500px";
    dialconf.maxWidth= "75%";

    const dialogRef = this.dialog.open(InformasiPribadiComponent, dialconf);
  }

}
