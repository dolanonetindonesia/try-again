import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusHeadComponent } from './status-head.component';

describe('StatusHeadComponent', () => {
  let component: StatusHeadComponent;
  let fixture: ComponentFixture<StatusHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
