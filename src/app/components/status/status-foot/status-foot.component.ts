import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-status-foot',
  templateUrl: './status-foot.component.html',
  styleUrls: ['./status-foot.component.css']
})
export class StatusFootComponent implements OnInit {

  constructor(public translate: TranslateService) { 
    translate.use(localStorage['lang'])
  }

  ngOnInit() {
  }

}
