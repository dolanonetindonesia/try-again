import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusFootComponent } from './status-foot.component';

describe('StatusFootComponent', () => {
  let component: StatusFootComponent;
  let fixture: ComponentFixture<StatusFootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusFootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusFootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
