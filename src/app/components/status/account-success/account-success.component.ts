import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-success',
  templateUrl: './account-success.component.html',
  styleUrls: ['./account-success.component.css']
})
export class AccountSuccessComponent implements OnInit {

  constructor(public translate: TranslateService) { 
    translate.use(localStorage['lang'])
  }

  ngOnInit() {
  }

}
