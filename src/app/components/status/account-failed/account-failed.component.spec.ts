import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountFailedComponent } from './account-failed.component';

describe('AccountFailedComponent', () => {
  let component: AccountFailedComponent;
  let fixture: ComponentFixture<AccountFailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountFailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
