import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account-failed',
  templateUrl: './account-failed.component.html',
  styleUrls: ['./account-failed.component.css']
})
export class AccountFailedComponent implements OnInit {

  public nama: string;

  constructor(
    private actvroute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.actvroute.params.subscribe(params => {
      this.nama = params['token']
    });
  }

}
