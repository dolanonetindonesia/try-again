import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-forget-password-change',
  templateUrl: './forget-password-change.component.html',
  styleUrls: ['./forget-password-change.component.css']
})
export class ForgetPasswordChangeComponent implements OnInit {

  changePassword: FormGroup;
  hide_password = true;
  temp_password = "";

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm()
  }

  createForm(){
    this.changePassword = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      konfirmasi: ['', [Validators.required, this.cekPassword()]]
    })
  }

  passKeyUp(event: any){
    this.temp_password = event.target.value;
  }
  cekPassword(){
    return (ac: AbstractControl): { [key: string]: boolean } | null => {
      if(ac.value !== undefined && ((ac.value).toString() !== this.temp_password.toString())){
        return { 'missMatch': true }
      }
      return null;
    }
  }

}
