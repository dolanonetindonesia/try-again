import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgetPasswordChangeComponent } from './forget-password-change.component';

describe('ForgetPasswordChangeComponent', () => {
  let component: ForgetPasswordChangeComponent;
  let fixture: ComponentFixture<ForgetPasswordChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgetPasswordChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgetPasswordChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
