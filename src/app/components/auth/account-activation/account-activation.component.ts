import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { PartnerService } from '../../../services/partner.service';

@Component({
  selector: 'app-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.css']
})
export class AccountActivationComponent implements OnInit {

  constructor(
    private actvroute: ActivatedRoute,
    private partnerSvc: PartnerService,
    private router: Router,
    private authScv: AuthService
  ) { }

  ngOnInit() {
    this.actvroute.params.subscribe(params => {
      this.createCredential(params['token'])
    });
  }
  
  createCredential(token){
    console.log(token)
    this.partnerSvc.addPartnerCredential(token)
        .subscribe(data => {
          if(data['status']){
            console.log(data['data'])
            this.router.navigate(['/registration/status/account-success', { s: data['data'] }]);
          } else{
            console.log(data['data'])
            this.router.navigate(['/registration/status/account-failed', { s: data['message'] }]);
          }
        }, error =>{
          console.log(error)
        })
  }

}
