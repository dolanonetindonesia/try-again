import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl, FormGroupDirective, NgForm, ValidatorFn } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ForgetPasswordComponent } from '../forget-password/forget-password.component';
import { PendaftaranAwalPersonalComponent } from '../../tnc/pendaftaran-awal-personal/pendaftaran-awal-personal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logPartner: FormGroup;
  hide_password = true;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private authSvc: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.logPartner = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      role: ['partner']
    });
  }

  forget_password(){
    const dialconf = new MatDialogConfig();
    dialconf.maxWidth= "80%";
    const dialogRef = this.dialog.open(ForgetPasswordComponent, dialconf);
  }

  onSubmit(){
    console.log('submited');
    // if(data.email && data.password){
      this.authSvc.login(this.logPartner.value)
        .subscribe(
        //   data => {
        //   console.log('user logged in')
        //   console.log(data)
        //   this.router.navigateByUrl('/dashboard/home')
        // }
          res => {
            localStorage.setItem('token', res.data)
          },
          err => console.log(err)
        )
    // }    
  }

}
