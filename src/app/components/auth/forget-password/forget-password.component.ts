import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  forgetPassword: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm()
  }

  createForm(){
    this.forgetPassword = this.fb.group({
      email: ['', Validators.required]
    })
  }

  onSubmit(){
    console.log('submitted')
    console.log(this.forgetPassword.value)
  }

}
