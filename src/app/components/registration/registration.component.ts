import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public lang: string;

  constructor(
    public dialog: MatDialog,
    public translate: TranslateService
  ) { 
    translate.addLangs(['en', 'id']);
    translate.setDefaultLang('id');  
    if(localStorage['lang']){
      translate.use(localStorage['lang'])
    } else{
      const browserLang = translate.getBrowserLang();
      this.lang = browserLang;
      localStorage['lang'] = browserLang;
      translate.use(browserLang.match(/en|id/) ? browserLang : 'en')
    }
  }

  ngOnInit() {
  }

  setLanguage(lang: string){
    this.translate.use(lang);
    localStorage['lang'] = lang;
  }

}


/*
*
CUSTOM PASSWORD CONFIRMATION VALIDATOR
*
*/
// export class ParentErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//       const isSubmitted = !!(form && form.submitted);
//       const controlTouched = !!(control && (control.dirty || control.touched));
//       const controlInvalid = !!(control && control.invalid);
//       const parentInvalid = !!(control && control.parent && control.parent.invalid && (control.parent.dirty || control.parent.touched));

//       return isSubmitted || (controlTouched && (controlInvalid || parentInvalid));
//   }
// }

// export function ValidatePasswordConfirmation(group: FormGroup) {
//   if(group.value.password !== group.value.konfirmasi){
//     // console.log(group.value.password);
//       return { 'no-match':true };
//   }
//   return null;
// }

// export function ageRangeValidator(min: number, max: number): ValidatorFn{
//   return (ac: AbstractControl): { [key: string]: boolean } | null => {
//     if(ac.value !== undefined && (isNaN(ac.value) || ac.value < min || ac.value > max)){
//       return { 'ageRange': true }
//     }
//     return null;
//   }
// }

// export function cekPassword(){
//   return (ac: AbstractControl): { [key: string]: boolean } | null => {
//     console.log(ac.value+" : "+this.temp_password);
//     if(ac.value !== undefined && (isNaN(ac.value) || ac.value != this.temp_password)){
//       console.log("missmatch FALSE")
//       return { 'missMatch': true }
//     }
//     return null;
//   }
// }