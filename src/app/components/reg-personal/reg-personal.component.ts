import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl,  ValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { EmailService } from '../../services/email.service';
import { PartnerService } from '../../services/partner.service';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { PendaftaranAwalPersonalComponent } from '../tnc/pendaftaran-awal-personal/pendaftaran-awal-personal.component';

@Component({
  selector: 'app-reg-personal',
  templateUrl: './reg-personal.component.html',
  styleUrls: ['./reg-personal.component.css']
})
export class RegPersonalComponent implements OnInit {

  regPartnerIndividu: FormGroup;
  mailErr = false;
  data = {} as any;
  hide_password = true;
  temp_password = "";

  partnerTypes = [
    {
      id: 1,
      tipe: "Guide"
    },
    {
      id: 2,
      tipe: "Activity"
    },
    {
      id: 3,
      tipe: "Site"
    }
  ]

  

  constructor(
    private fb: FormBuilder,
    private emailSvc: EmailService,
    private partnerSvc: PartnerService,
    public dialog: MatDialog,
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  tnc_personal(){
    const dialconf = new MatDialogConfig();

    // dialconf.maxHeight = "530px";
    dialconf.maxWidth= "50%";

    const dialogRef = this.dialog.open(PendaftaranAwalPersonalComponent, dialconf);
  }


  createForm() {
    this.regPartnerIndividu = this.fb.group({
      // age: ['', [Validators.required, this.ageRangeValidator(4,9)]],
      tipe_partner: ['', Validators.required],
      first_name: ['', Validators.required ],
      last_name: ['', Validators.required ],
      phone_number: ['', [Validators.required, Validators.pattern("[0-9]+")]],
      email: ['', [Validators.required, Validators.email, this.cekEmail()]],
      password: ['', [Validators.required,Validators.minLength(6)]],
      konfirmasi: ['', [Validators.required, this.cekPassword()]],
      cb: [false, Validators.requiredTrue],
      is_corporate: [0]
    });
  }

  onSubmit(){
    let data = this.regPartnerIndividu.value;
    delete data['konfirmasi'];
    delete data['cb'];
    this.partnerSvc.regPartner(data)
        .subscribe(data => {
          if(data['status']){
            console.log('berhasil, silahkan cek email')
            console.log(data['message'])
            this.router.navigate(['/registration/status/register-success', { status: data['status'] }]);
          } else{
            console.log('gagal, coba lagi')
            console.log(data['message'])
            this.router.navigate(['/registration/status/register-failed', { status: data['status'] }]);
          }
        });
    console.log('submited');
  }

  cekEmail(): ValidatorFn{
    return (ac: AbstractControl): { [key: string]: boolean } | null => {
      if(ac.value !== undefined && isNaN(ac.value)){
        this.emailSvc.cekEmail(ac.value)
            // .pipe(map(data => stat = data))
            .subscribe(data => {
              if(data['status']){
                console.log('email sudah terpakai')
                console.log(data['message'])
                return {'emailSudahAda': true }
              } else{
                console.log(data['message'])
                console.log('email bisa digunakan')
                return null
              }
            })
      }
      return null;
    }
  }

  passKeyUp(event: any){
    this.temp_password = event.target.value;
  }
  cekPassword(){
    return (ac: AbstractControl): { [key: string]: boolean } | null => {
      // console.log(this.temp_password+" : "+ac.value)
      if(ac.value !== undefined && ((ac.value).toString() !== this.temp_password.toString())){
        return { 'missMatch': true }
      }
      return null;
    }
  }

  /*
  ageRangeValidator(min: number, max: number): ValidatorFn{
    return (ac: AbstractControl): { [key: string]: boolean } | null => {
      if(ac.value !== undefined && (isNaN(ac.value) || ac.value < min || ac.value > max)){
        return { 'ageRange': true }
      }
      return null;
    }
  }
  */

}
