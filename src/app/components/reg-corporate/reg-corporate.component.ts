import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl, FormGroupDirective, NgForm, ValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { EmailService } from '../../services/email.service';
import { PartnerService } from '../../services/partner.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { PendaftaranAwalCorporateComponent } from '../tnc/pendaftaran-awal-corporate/pendaftaran-awal-corporate.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reg-corporate',
  templateUrl: './reg-corporate.component.html',
  styleUrls: ['./reg-corporate.component.css']
})
export class RegCorporateComponent implements OnInit {

  corpTypes = [
    {
      id: 1,
      nama: 'PT'
    },
    {
      id: 2,
      nama: 'CV'
    },
    {
      id: 3,
      nama: 'Koperasi'
    }
  ];
  partnerTypes = [
    {
      id: 1,
      tipe: "Guide"
    },
    {
      id: 2,
      tipe: "Activity"
    },
    {
      id: 3,
      tipe: "Site"
    }
  ]
  regPartnerPerusahaan: FormGroup;
  data = {} as any;
  hide_password = true;
  temp_password = "";

  constructor(
    private fb: FormBuilder,
    private emailSvc: EmailService,
    public dialog: MatDialog,
    private partnerSvc: PartnerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm()
  }

  createForm() {
    this.regPartnerPerusahaan = this.fb.group({
      tipe_partner: ['', Validators.required],
      corp_name: ['', Validators.required ],
      corp_status: ['', Validators.required ],
      corp_phone_number: ['', [Validators.required, Validators.pattern("[0-9]+")]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,Validators.minLength(6)]],
      konfirmasi: ['', [Validators.required, this.cekPassword()]],
      cb: [false, Validators.requiredTrue],
      is_corporate: [1]
    });
  }

  tnc_corporate(){
    const dialconf = new MatDialogConfig();

    dialconf.maxHeight = "530px";
    dialconf.maxWidth= "80%";

    const dialogRef = this.dialog.open(PendaftaranAwalCorporateComponent, dialconf);
  }

  passKeyUp(event: any){
    this.temp_password = event.target.value;
  }
  cekPassword(){
    return (ac: AbstractControl): { [key: string]: boolean } | null => {
      console.log(this.temp_password+" : "+ac.value)
      if(ac.value !== undefined && ((ac.value).toString() !== this.temp_password.toString())){
        return { 'missMatch': true }
      }
      return null;
    }
  }

  onSubmit(){
    let data = this.regPartnerPerusahaan.value;
    delete data['konfirmasi'];
    delete data['cb'];
    this.partnerSvc.regPartner(data)
        .subscribe(data => {
          if(data['status']){
            console.log(data['message'])
            this.router.navigate(['/registration/status/register-success', { status: data['status'] }]);
          } else{
            console.log('gagal, coba lagi')
            console.log(data['message'])
            this.router.navigate(['/registration/status/register-failed', { status: data['status'] }]);
          }
        });
    console.log('submited')
  }

}
