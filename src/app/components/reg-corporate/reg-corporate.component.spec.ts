import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegCorporateComponent } from './reg-corporate.component';

describe('RegCorporateComponent', () => {
  let component: RegCorporateComponent;
  let fixture: ComponentFixture<RegCorporateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegCorporateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegCorporateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
